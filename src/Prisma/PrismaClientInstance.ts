import { PrismaClient } from "@prisma/client";

let client: PrismaClient;

if (process.env.NODE_ENV === "production") {
  client = new PrismaClient();
} else {
  // @ts-ignore: Ignore TypeScript error about global.client not having an index signature would do "as any but this made more sense to me."
  if (!global.client) {
    // @ts-ignore: Ignore TypeScript error about global.client not having an index signature would do "as any but this made more sense to me."
    global.client = new PrismaClient();
  }
  // @ts-ignore: Ignore TypeScript error about global.client not having an index signature would do "as any but this made more sense to me."
  client = global.client;
}

export default client;
