export type OriginalTrack = {
  trackRef: string;
  Type: "movie" | "series" | string;
  title: string;
  Rated: string;
  posterUrl: string;
  cardImg: string;
  description: string;
  original: boolean;
  dateAdded: string;
  newSeason: boolean;
  oscarNominee: boolean;
  oscarWinner: boolean;
  Genre: string[];
  Tags: string[];
  Top10M?: number;
  Top10S?: number;
};

export type MergedTrack = Omit<
  OriginalTrack,
  "title" | "posterUrl" | "description" | "Genre"
> & {
  Title: string;
  Genre?: string;
  Year?: string;
  Released?: string;
  Runtime?: string;
  Director: string;
  Writer: string;
  Actors?: string;
  Plot: string;
  Language?: string;
  Country?: string;
  Awards?: string;
  Ratings?: { Source: string; Value: string }[];
  imdbRating?: string;
  imdbVotes?: string;
  totalSeasons?: string;
};
// trackRef relates to imdbID

export type OMDBTrack = {
  Title: string;
  Year: string;
  Rated: string;
  Released: string;
  Runtime: string;
  Genre: string;
  Director: string;
  Writer: string;
  Actors: string;
  Plot: string;
  Language: string;
  Country: string;
  Awards: string;
  Poster: string;
  Ratings: Array<{ Source: string; Value: string }>;
  Metascore: string;
  imdbRating: string;
  imdbVotes: string;
  imdbID: string;
  Type: string;
  totalSeasons?: string;
  Response: string;
} | null;

export type MediaCategory = {
  id: string;
  name: string;
  description: string;
  varient: string;
  trackRefs: string[];
  Tags?: string[];
};
