"use client";
import { create } from "zustand";
type VideoData = {
  key: string;
  path: string;
  videoSrc: string;
  trackRef: string;
  imageRef: string;
  loading: boolean;
};

type VideoStoreProps = {
  videoData: VideoData;
  setVideoData: (data: VideoData) => void;
  fetchVideoSource: (path: string) => void;
};

export const useVideoStore = create<VideoStoreProps>((set) => ({
  videoData: {
    key: "",
    path: "",
    videoSrc: "",
    trackRef: "",
    imageRef: "",
    loading: true,
  },
  setVideoData: (data) => set({ videoData: { ...data, loading: false } }),
  fetchVideoSource: (pathName: string) => {
    let videoSrc: string;
    let imageRef: string;
    let trackRef: string;
    let path: string;
    let key: string;

    switch (pathName) {
      case "/browse":
        path = pathName;
        key = "default";
        videoSrc =
          "https://apdn770wtmsj7kcc.public.blob.vercel-storage.com/Sprung-iwwfxfG5k609BZwwBDrwgz8CAVKi3j.mp4";
        imageRef = "/static/Film/Sprung.png";
        trackRef = "tt14544190";
        break;
      case "/browse/tv":
        path = pathName;
        key = "tv";
        videoSrc =
          "https://apdn770wtmsj7kcc.public.blob.vercel-storage.com/Sprung-iwwfxfG5k609BZwwBDrwgz8CAVKi3j.mp4";
        imageRef = "/static/Film/Sprung.png";
        trackRef = "tt14544190";
        break;
      case "/browse/movies":
        path = pathName;
        key = "movies";
        videoSrc =
          "https://apdn770wtmsj7kcc.public.blob.vercel-storage.com/BeKind-Xucpwe38M3K7nfdzriFCwRZpoJQyW9.mp4";
        imageRef = "/static/Film/BeKind.jpg";
        trackRef = "tt0799934";
        break;
      case "/latest":
        path = pathName;
        key = "latest";
        videoSrc =
          "https://apdn770wtmsj7kcc.public.blob.vercel-storage.com/Sprung-iwwfxfG5k609BZwwBDrwgz8CAVKi3j.mp4";
        imageRef = "/static/Film/Sprung.png";
        trackRef = "tt14544190";
        break;
      default:
        path = pathName;
        key = "default";
        videoSrc =
          "https://apdn770wtmsj7kcc.public.blob.vercel-storage.com/BeKind-Xucpwe38M3K7nfdzriFCwRZpoJQyW9.mp4";
        imageRef = "/static/Film/BeKind.jpg";
        trackRef = "tt0799934";
        break;
    }

    set({
      videoData: {
        key,
        path,
        videoSrc,
        imageRef,
        trackRef,
        loading: false,
      },
    });
  },
}));
