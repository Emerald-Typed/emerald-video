"use client";
import { Carousel } from "@mantine/carousel";
import { Container, Title, Transition, Text, Group } from "@mantine/core";
import { BsChevronRight, BsChevronLeft } from "react-icons/bs";
import { useHover } from "@mantine/hooks";
import { Suspense } from "react";
import { BaseCard } from "@/app/components/Cards/BaseCard/BaseCard";
import { MediaCategory } from "@/lib/types";
import { SkeletonCard } from "@/app/components/Skelatons/SkelCard";
import { useVideoStore } from "../Helpers/videoProvider";

export const SinglularCarousel = ({ Slide }: { Slide: MediaCategory }) => {
  const { hovered, ref } = useHover();
  const { videoData } = useVideoStore();
  const exploreRef = `/browse/m/genre/${Slide.id}-${videoData.key}`;

  return (
    <Container size="responsive" key={Slide.name}>
      <Group className="text-white group" ref={ref}>
        <Title py="sm" order={2}>
          {Slide.name}
        </Title>
        <a href={exploreRef} style={{ textDecoration: "none" }}>
          <Text className="text-teal-200 flex items-center">
            <Transition
              mounted={hovered}
              transition="slide-right"
              duration={300}
              timingFunction="ease"
            >
              {(styles) => <span style={styles}>Explore More</span>}
            </Transition>
            <BsChevronRight size={15} className="ml-2" />
          </Text>
        </a>
      </Group>
      <Carousel
        height="200px"
        slideGap="md"
        align={window.innerWidth < 750 ? "center" : "start"}
        slideSize={{ lg: "16.666667%", md: "33%", sm: "70%", base: "100%" }}
        withControls
        withIndicators={true}
        loop
        draggable={false}
        nextControlIcon={
          <BsChevronRight
            style={{
              height: 20,
              width: 20,
              position: "relative",
              right: 0,
              top: 0,
              boxShadow: "none",
              borderRadius: 0,
            }}
          />
        }
        previousControlIcon={
          <BsChevronLeft
            style={{
              height: 20,
              width: 20,
              position: "relative",
              left: 0,
              top: 0,
              boxShadow: "none",
              borderRadius: 0,
            }}
          />
        }
      >
        {Slide.trackRefs.map((track, index) => (
          <Suspense
            key={index}
            fallback={
              <Carousel.Slide key={track}>
                <SkeletonCard />
              </Carousel.Slide>
            }
          >
            <Carousel.Slide key={track}>
              <BaseCard trackref={track} />
            </Carousel.Slide>
          </Suspense>
        ))}
      </Carousel>
    </Container>
  );
};
