"use client";
import { useState, useEffect } from "react";
import { Container, Group, Image, Stack, AppShell } from "@mantine/core";

function HeaderLogin() {
  const [scrolled, setScrolled] = useState(false);
  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY > 50) {
        setScrolled(true);
      } else {
        setScrolled(false);
      }
    };
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);
  return (
    <AppShell withBorder={false} header={{ height: 60 }}>
      <AppShell.Header
        zIndex={1001}
        className={`transition duration-500 px-2 ${
          scrolled ? "bg-emerald-950/80" : "bg-transparent"
        }`}
      >
        <Stack align="stretch" h={60} justify="center" gap="xs">
          <Group justify="flex-start" wrap="nowrap">
            <Image
              className="rounded-sm hidden lg:block"
              h={40}
              w="auto"
              src="../static/Film/EmeraldVideo.png"
              alt="Home"
            />
            <Image
              className="rounded-sm block lg:hidden"
              h={30}
              w="auto"
              src="../static/Film/E.png"
              alt="Home"
            />
          </Group>
        </Stack>
      </AppShell.Header>
    </AppShell>
  );
}

export default HeaderLogin;
