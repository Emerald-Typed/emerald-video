"use client";
import { Title, Button, Image, Paper } from "@mantine/core";
import HeaderLogin from "@/app/components/Nav/HeaderLogin";
import Footer from "@/app/components/Nav/Footer";
import { useEffect } from "react";

export default function Error({
  error,
  reset,
}: {
  error: Error & { digest?: string };
  reset: () => void;
}) {
  useEffect(() => {
    console.error(error);
  }, [error]);

  return (
    <div className="relative h-screen bg-rose-900/20 ERRORTEXT">
      <HeaderLogin />
      <div className="flex items-center justify-center h-full relative z-10">
        <Paper
          shadow="xs"
          pb="sm"
          px="md"
          className="lg:max-w-2xl md:max-w-xl max-w-sm w-full text-center"
        >
          <Image
            src="/static/WhoopsTranspo.png"
            style={{
              borderBottom: "2px solid #10b981",
            }}
          />
          <Title className="text-center">Whoopsy Daisy!</Title>{" "}
          <Title className="text-center" size="sm">
            Something went wrong somewhere care to try again{" "}
          </Title>
          <Button onClick={() => reset()} mt="sm" fullWidth color="emerald.6">
            Reset
          </Button>
        </Paper>
      </div>
      <Footer />
    </div>
  );
}
