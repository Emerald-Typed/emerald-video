"use client";
import { Suspense } from "react";
import { useSearchParams } from "next/navigation";
import { BaseCardStatic } from "@/app/components/Cards/BaseNoGrow";
import { SimpleGrid, Container, Title, Skeleton } from "@mantine/core";
import { InfoCard } from "@/app/components/Cards/InfoCard";
import { SkeletonCard } from "@/app/components/Skelatons/SkelCard";
import { useListStore } from "@/app/components/Helpers/listProvider";
export default function Page() {
  const searchParams = useSearchParams();
  const id = searchParams.get("jbv");
  return (
    <div className="min-h-screen py-10 pt-[65px] w-full px-2">
      <Suspense
        fallback={
          <>
            <Skeleton height={85} width="100%" />
            <SimpleGrid pt="md" cols={{ base: 1, sm: 2, lg: 6 }}>
              {Array.from({ length: 6 }, (_, index) => (
                <SkeletonCard key={index} />
              ))}
            </SimpleGrid>
          </>
        }
      >
        <Grid />
      </Suspense>
      {id && (
        <Suspense fallback={null}>
          <InfoCard id={id} />
        </Suspense>
      )}
    </div>
  );
}
const Grid = () => {
  const { list } = useListStore();

  return (
    <>
      <Container
        fluid
        className="my-2 py-2 space-y-2 bg-gradient-to-r rounded-md from-emerald-500 to-emerald-800 text-white"
      >
        <Title order={1}>My List</Title>
      </Container>

      <SimpleGrid cols={{ base: 1, sm: 2, lg: 6 }}>
        {list.map((trackref: string) => (
          <BaseCardStatic key={trackref} trackref={trackref} />
        ))}
      </SimpleGrid>
    </>
  );
};
