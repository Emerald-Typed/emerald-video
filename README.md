<a name="readme-top"></a>
<br />

<div align="center">
<a href="https://emerald-typed.vercel.app/">
<img src="https://gitlab.com/Emerald-Typed/readme/-/raw/main/public/LOGO.png" alt="Logo">
</a>
<h3 align="center">Emerald Video V2</h3>
<p align="center">
A Netflix Clone made with Mantine, with matching paths and routes
<br />
<a href="https://emerald-video.vercel.app/"><strong>View Deployment  »</strong></a>
<br />
<a href="https://gitlab.com/Emerald-Typed/emerald-video"><strong>Explore the Repo »</strong></a>
<br />
<a href="https://emerald-typed.vercel.app/contact">Contact Me</a>
·
<a href="https://gitlab.com/Emerald-Typed/emerald-video/issues">Report Bugs</a>
·
<a href="https://gitlab.com/Emerald-Typed/emerald-video/issues">Request Feature</a>
</p>
</br>

[![Last Commit][commit-badge]](https://gitlab.com/Emerald-Typed/emerald-video)

</div>

## Table of Contents

#### [About](#about-the-project)

#### [ScreenShot](#screenshot)

#### [Features](#features)

#### [Libraries](#built-with)

#### [Usage](#usage)

#### [Contact](#contact)

#### [License](#license)

## About The Project

I spent this September building a Netflix Clone with Mantine.

It not only mimics the UI but also the basepath and structure of the application.

I took a video of the old version see public /Legacy.mp4, and I'm proud of how much I've improved it.

The focus was mostly on interactive UI and caching static data. I used React Query for caching and suspense, and Zustand for state management. I also used Clerk for authentication and Zod for form validation.

### ScreenShot

![SignIn][Screenshot-SignIn]
![Browse][Screenshot-Browse]
![Search][Screenshot-Search]
![List][Screenshot-List]
![Info][Screenshot-Info]
![Shimmer][ScreenShot-Shimmer]
![Mobile][ScreenShot-Mobile]

</br>

## Features

- **Sign In/Sign Up:** I was able to grab the background image used on Netflix from their source, then I made a Mantine Zod simple form to handle the sign-in and sign-up. NOTE: Netflix sign-up is multistep, potentially going to a different domain. I plan to implement a UI change to mimic this in the future.
- **Matching Header:** Navigation between pages is nearly identical in UI to Netflix, dynamic, and follows the same paths. It also mimics the mobile menu, NO MATTER WHAT VIEWPORT! Try inspecting and viewing on mobile.
- **The Carousels Match Closely to Netflix's UI:** This was arguably the hardest part of this whole project. I like Mantine's controls and how they handle state, but I had to write a lot of custom CSS to get the carousels to look and feel like Netflix's. The buttons match the full height, allow unique overflow, and ensure that the card from the current carousel renders above the lower carousel on hover.
- **Shimmer Skeletons for Better Loading Experience:** I implemented Mantine Skeletons for various components like the info card, regular carousel card, carousel itself, and the background video. Since Next.js `useSearchParams` is at the top of the app and uses suspense, I created a lot of shimmer skeletons that integrate with React Query's client for handling loading states. This ensures the app shows smooth loading animations while data is fetched, improving the user experience.
- **Matching URI Paths:** Netflix's sign-in path is (/), sign-up path is (/sign-up), browse base is (/browse), search with one character in the search bar goes to (/search?q={value}), New and Popular is (/latest), and the routes under browse are (/browse/my-list, /browse/tv, /browse/movies). Any of the regular navigations can have /browse?jbv={trackref}, allowing the info card to display.
- **Dynamic Serving of Video Background and Page Categories by Path:** A Zustand store sets the background video source and page categories for carousels based on the pathname. This allows categories for movies, TV shows, and 'latest.' In the future, this could be hooked up to a database and made unique to a UserId.
- **Search Bar with Redirect:** When a value exists in the search bar, it pushes /search?q={yourValueinBar}. On clearing, it returns you to your original path, e.g., /browse/tv.
- **User Like Button:** The like button interactions (Like, Double Like, Dislike) mimic Netflix, but only the like button is always visible. The others grow to the right on hover. I wrote some CSS to ensure it grows correctly and renders properly next to the movie info, even on small viewports.
- **Caching All Media Data:** I am using Tanstack React Query with suspense for my carousel and card fetches, and to cache as much data as possible to avoid pulling every movie and TV show on every page render. For performance, I only fetch what's necessary, like if a track is in a user's list.
- **Background Video:** I downloaded video clips from IMDb and hosted them on Vercel BLOB. They autoplay with a stateful mute button. NOTE: Mobile viewports default to a poster image, as autoplay doesn't work on mobile browsers and it looks cleaner this way.
- **Pretty Awesome Video Library:** I curated a list of mock data from OMDB, using titles from my own library. I added Netflix Original and Netflix Exclusive tags to the data. Check my libs folder for the data.

</br>

## Libraries

### Built With

[![Next_JS](https://img.shields.io/badge/next.js-34d399?logo=next.js&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge)](https://nextjs.org/) [![React](https://img.shields.io/badge/react-34d399?logo=react&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge)](https://react.dev/) [![Prisma](https://img.shields.io/badge/prisma-34d399?logo=prisma&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge)](https://www.prisma.io/) [![PostgreSQL](https://img.shields.io/badge/postgresql-34d399?logo=postgresql&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge)](https://www.postgresql.org/) [![TypeScript](https://img.shields.io/badge/typescript-34d399?logo=typescript&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge)](https://www.typescriptlang.org/) [![Clerk](https://img.shields.io/badge/clerk-34d399?logo=clerk&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge)](https://clerk.dev/) [![Zod](https://img.shields.io/badge/zod-34d399?logo=zod&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge)](https://zod.dev/) [![Tailwind_CSS](https://img.shields.io/badge/tailwindcss-34d399?logo=tailwindcss&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge)](https://tailwindcss.com/) [![Google_reCAPTCHA](https://img.shields.io/badge/google-34d399?logo=google&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge)](https://www.google.com/recaptcha/) [![Mantine](https://img.shields.io/badge/mantine-34d399?logo=mantine&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge)](https://mantine.dev/) [![Node_JS](https://img.shields.io/badge/node.js-34d399?logo=node.js&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge)](https://nodejs.org/en/) [![Zustand](https://img.shields.io/badge/npm-34d399?logo=npm&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge)](https://zustand.docs.pmnd.rs/)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## Usage

1. **Sign Up/Sign In:** The website defaults to the login page under path (/), and you can click sign up to go to the sign-up page (/sign-up).
1. **Browse:** Just click around the library! The data is from some of my favorite movies and TV shows, scraped from OMDB and my personal library. You can like, add to the list, get more info on a track, or explore more on categories.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## Roadmap:

- **Prisma Find Many for Search:** I want to use Prisma's 'findMany' to search for the user's list and categories of movies and TV shows. I'd like to display the first 10 or 20 results, then load more on scroll or pagination.
- **Multi-Step Sign-Up:** I want to implement a multi-step sign-up process that mimics Netflix's. This will involve a lot of state management and potentially a new domain. But didn't want the proccess to test my app to be too long.

</br>

## Contact

Kyle Hipple - [Emerald-Typed Gitlab](https://gitlab.com/Emerald-Typed) - [Contact Me](https://emerald-typed.vercel.app/contact)

Project Link: [https://gitlab.com/Emerald-Typed/emerald-video](https://gitlab.com/Emerald-Typed/emerald-video)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## License

All Rights Reserved License. See [License.txt](https://gitlab.com/Emerald-Typed/emerald-video/-/raw/main/LICENSE.txt)
for more information.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

[commit-badge]: https://img.shields.io/gitlab/last-commit/Emerald-Typed/emerald-video?label=last%20commit&style=for-the-badge&color=34d399&labelColor=%23a1a1aa

[Screenshot-SignIn]: https://gitlab.com/Emerald-Typed/emerald-video/-/raw/main/public/static/Screenshot-SignIn.png
[Screenshot-Browse]: https://gitlab.com/Emerald-Typed/emerald-video/-/raw/main/public/static/Screenshot-Browse.png
[Screenshot-Search]: https://gitlab.com/Emerald-Typed/emerald-video/-/raw/main/public/static/Screenshot-Search.png
[Screenshot-List]: https://gitlab.com/Emerald-Typed/emerald-video/-/raw/main/public/static/Screenshot-List.png
[Screenshot-Info]: https://gitlab.com/Emerald-Typed/emerald-video/-/raw/main/public/static/Screenshot-Info.png
[ScreenShot-Shimmer]: https://gitlab.com/Emerald-Typed/emerald-video/-/raw/main/public/static/Screenshot-Shimmer.png
[ScreenShot-Mobile]: https://gitlab.com/Emerald-Typed/emerald-video/-/raw/main/public/static/Screenshot-Mobile.png
