import { MergedTrack } from "@/lib/types";
import OriginalDB from "@/lib/FinalSeedEmerald.json";

//helps me build categories until i hook up prisma
const items: MergedTrack[] = OriginalDB;

const genres = process.argv.slice(2);
const type = process.argv[process.argv.length - 1];

const carouselQuery = (genres: string[], type?: string) => {
  console.log("Input Genres:", genres);
  const filteredItems = items.filter((item) => {
    console.log("Checking item:", item);
    const itemGenres =
      typeof item.Genre === "string"
        ? item.Genre.split(",").map((genre) => genre.trim())
        : [];
    const genreMatch = genres.some((genre) => itemGenres.includes(genre));
    const typeMatch = type ? item.Type === type : true;
    return genreMatch && typeMatch;
  });

  const results = filteredItems.map((item) => ({
    trackRef: item.trackRef,
    title: item.Title,
    genre: item.Genre,
    type: item.Type,
  }));

  console.log("Filtered Results:", results);
};

carouselQuery(genres, type.length ? type : undefined);
