"use client";
import { Suspense } from "react";
import { usePathname, useSearchParams } from "next/navigation";
import { BaseCardStatic } from "@/app/components/Cards/BaseNoGrow";
import { SimpleGrid, Title, Container, Skeleton } from "@mantine/core";
import { InfoCard } from "@/app/components/Cards/InfoCard";
import { SkeletonCard } from "@/app/components/Skelatons/SkelCard";
import { SkelInfo } from "@/app/components/Skelatons/SkelInfo";
import { fetchCarousel } from "@/React-Query/TypedFetch";
import { useSuspenseQuery } from "@tanstack/react-query";

export default function Page({ params }: { params: { combined: string } }) {
  const searchParams = useSearchParams();
  const data = params.combined.split("-");
  const genre = parseInt(data[0]);
  const path = data[1];
  const moreToggeleTemp = searchParams.get("jbv");

  return (
    <>
      {moreToggeleTemp && (
        <Suspense fallback={<SkelInfo />}>
          <InfoCard id={moreToggeleTemp} />
        </Suspense>
      )}
      <div className="min-h-screen py-10 pt-[65px] w-full px-2">
        <Suspense
          fallback={
            <>
              <Skeleton height={85} width="100%" />
              <SimpleGrid cols={{ base: 1, sm: 2, lg: 6 }}>
                <SkeletonCard />
                <SkeletonCard />
                <SkeletonCard />
                <SkeletonCard />
                <SkeletonCard />
                <SkeletonCard />
              </SimpleGrid>
            </>
          }
        >
          <Grid genreData={{ id: genre, path: path }} />
        </Suspense>
      </div>
    </>
  );
}
const Grid = ({ genreData }: { genreData: { id: number; path: string } }) => {
  const pathname = usePathname();
  console.log(genreData, pathname);
  const { data: categories } = useSuspenseQuery({
    queryKey: ["carousels"],
    queryFn: () => fetchCarousel(genreData.path),
    staleTime: Infinity,
  });

  return (
    <>
      <Container
        fluid
        className="my-2 py-2 space-y-2 bg-gradient-to-r rounded-md from-emerald-500 to-emerald-800 text-white"
      >
        <Title order={1}>{categories[genreData.id].name}</Title>
        <Title order={3} fw={400}>
          {categories[genreData.id].description}
        </Title>
      </Container>
      <SimpleGrid cols={{ base: 1, sm: 2, lg: 6 }}>
        {categories[genreData.id].trackRefs.map((trackref: string) => (
          <Suspense key={trackref} fallback={<SkeletonCard />}>
            <BaseCardStatic key={trackref} trackref={trackref} />
          </Suspense>
        ))}
      </SimpleGrid>
    </>
  );
};
