import { NextResponse } from "next/server";
import { auth, BASE_PATH } from "@/Next-Auth/auth";
export const config = {
  matcher: ["/((?!api|_next/static|_next/image|.*\\.png$|.*\\.jpg$).*)"],
};

export default auth((req) => {
  const reqUrl = new URL(req.url);
  if (!req.auth && reqUrl?.pathname !== "/" && reqUrl?.pathname !== "/signup") {
    return NextResponse.redirect(
      new URL(
        `${BASE_PATH}/signin?callbackUrl=${encodeURIComponent(
          reqUrl?.pathname
        )}`,
        req.url
      )
    );
  }
  if (
    (req.auth && reqUrl?.pathname === "/") ||
    (req.auth && reqUrl?.pathname === "/signup")
  ) {
    return NextResponse.redirect(new URL(`/browse`, req.url));
  }
});
