"use client";
import { CiCircleInfo, CiPlay1 } from "react-icons/ci";
import Link from "next/link";
import {
  Stack,
  Title,
  Text,
  Button,
  Group,
  Image,
  ActionIcon,
} from "@mantine/core";
import React, { useState, useRef, Suspense, useEffect } from "react";
import { CiVolumeMute, CiVolumeHigh } from "react-icons/ci";
import { useSuspenseQuery } from "@tanstack/react-query";
import { fetchTrack } from "@/React-Query/TypedFetch";

type VideoData = {
  videoSrc: string;
  trackRef: string;
  imageRef: string;
};

const BGVideo = ({ videoData }: { videoData: VideoData }) => {
  const [isMuted, setIsMuted] = useState(true);
  const [isVideoEnded, setIsVideoEnded] = useState(false);
  const [windowWidth, setWindowWidth] = useState<null | number>(null);

  useEffect(() => {
    if (typeof window !== "undefined") {
      setWindowWidth(window.innerWidth);

      const handleResize = () => {
        setWindowWidth(window.innerWidth);
      };

      window.addEventListener("resize", handleResize);

      return () => window.removeEventListener("resize", handleResize);
    }
  }, []);
  const videoRef = useRef<HTMLVideoElement | null>(null);
  const toggleMute = () => {
    if (videoRef.current) {
      videoRef.current.muted = !isMuted;
      setIsMuted(!isMuted);
    }
  };
  const handleVideoEnd = () => {
    setIsVideoEnded(true);
  };

  const { data: trackData } = useSuspenseQuery({
    queryKey: ["track", videoData.trackRef],
    queryFn: () => fetchTrack(videoData.trackRef),
  });

  if (windowWidth && windowWidth > 500) {
    return (
      <div
        key={videoData.videoSrc}
        style={{
          position: "relative",
          minHeight: "80vh",
          overflow: "hidden",
        }}
      >
        {!isVideoEnded ? (
          <video
            autoPlay
            playsInline
            muted={isMuted}
            onEnded={handleVideoEnd}
            style={{
              position: "absolute",
              top: 0,
              width: "100%",
              height: "100%",
              objectFit: "cover",
              zIndex: 1,
            }}
            ref={videoRef}
          >
            <source src={videoData.videoSrc} type="video/mp4" />
            Your browser does not support the video tag.
          </video>
        ) : (
          <Image
            src={videoData.imageRef}
            alt="background"
            style={{
              position: "absolute",
              top: 0,
              width: "100%",
              height: "100%",
              objectFit: "cover",
              zIndex: 0,
            }}
          />
        )}
        <Stack
          style={{ minHeight: "80vh", zIndex: 1, position: "relative" }}
          align="stretch"
          justify="flex-end"
          gap="md"
          p="xl"
        >
          <Suspense>
            <Title className="pt-24" variant="gradient" component="span">
              <Text inherit component="span" c="white">
                {trackData.Title}
              </Text>
            </Title>
            <Text c="white" size="xl">
              {trackData.Plot}
            </Text>
          </Suspense>

          <Group justify="space-between">
            <Group justify="flex-start">
              <Button
                component={Link}
                href={`/watch/${videoData.trackRef}`}
                color="gray"
                size="xl"
                variant="light"
                radius="md"
                className="bg-gray-200/90 text-black hover:bg-gray-200/80 hover:text-slate-800/90"
                leftSection={<CiPlay1 size={40} />}
              >
                Play
              </Button>
              <Button
                component={Link}
                href={`?jbv=${videoData.trackRef}`}
                color="gray.7"
                style={{ opacity: 0.8 }}
                variant="filled"
                leftSection={<CiCircleInfo size={40} />}
                size="xl"
                radius="md"
              >
                More Info
              </Button>
            </Group>
            {!isVideoEnded && (
              <Group justify="flex-end">
                <ActionIcon
                  radius="xl"
                  onClick={toggleMute}
                  size="xl"
                  variant="light"
                  className="bg-gray-200/90 text-black hover:bg-gray-200/80 hover:text-slate-800/90"
                >
                  {isMuted ? (
                    <CiVolumeMute size={24} />
                  ) : (
                    <CiVolumeHigh size={24} />
                  )}
                </ActionIcon>
              </Group>
            )}
          </Group>
        </Stack>
      </div>
    );
  }
  return (
    <div
      key={videoData.videoSrc}
      style={{
        position: "relative",
        minHeight: "80vh",
        overflow: "hidden",
      }}
    >
      <Image
        src={videoData.imageRef}
        alt="background"
        style={{
          position: "absolute",
          top: 0,
          width: "100%",
          height: "100%",
          objectFit: "cover",
          zIndex: 0,
        }}
      />

      <Stack
        style={{ minHeight: "80vh", zIndex: 1, position: "relative" }}
        align="stretch"
        justify="flex-end"
        gap="md"
        p="xl"
      >
        <Suspense>
          <Title className="pt-24" variant="gradient" component="span">
            <Text inherit component="span" c="white">
              {trackData.Title}
            </Text>
          </Title>
          <Text c="white" size="xl">
            {trackData.Plot}
          </Text>
        </Suspense>

        <Group justify="space-between">
          <Group justify="flex-start">
            <Button
              component={Link}
              href={`/watch/${videoData.trackRef}`}
              color="gray"
              size="xl"
              variant="light"
              radius="md"
              className="bg-gray-200/90 text-black hover:bg-gray-200/80 hover:text-slate-800/90"
              leftSection={<CiPlay1 size={40} />}
            >
              Play
            </Button>
            <Button
              component={Link}
              href={`?jbv=${videoData.trackRef}`}
              color="gray.7"
              style={{ opacity: 0.8 }}
              variant="filled"
              leftSection={<CiCircleInfo size={40} />}
              size="xl"
              radius="md"
            >
              More Info
            </Button>
          </Group>
        </Group>
      </Stack>
    </div>
  );
};

export { BGVideo };
