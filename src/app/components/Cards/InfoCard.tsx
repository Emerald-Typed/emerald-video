"use client";
import { CiPlay1 } from "react-icons/ci";
import { RiCloseCircleLine } from "react-icons/ri";
import {
  Button,
  BackgroundImage,
  Grid,
  Stack,
  Group,
  Card,
  Badge,
  ActionIcon,
  Text,
} from "@mantine/core";
import Link from "next/link";
import { useSuspenseQuery } from "@tanstack/react-query";
import { ListButton } from "@/app/components/Cards/BaseCard/ListButton";
import { fetchTrackLong } from "@/React-Query/TypedFetch";
import RatingJSON from "@/lib/ratings.json";
import { usePathname } from "next/navigation";
import { Suspense } from "react";
import { LikeButton } from "@/app/components/Cards/BaseCard/LikeButton";
type Rating = {
  name: string;
  description: string;
};
type Ratings = {
  [key: string]: Rating;
};

const close = <RiCloseCircleLine size={30} />;

const ratingMaturity = RatingJSON as Ratings;

const InfoCard = (prop: { id: string }) => {
  const { data: trackData } = useSuspenseQuery({
    queryKey: ["cardLong", prop.id],
    queryFn: () => fetchTrackLong(prop.id),
  });
  const pathname = usePathname();
  return (
    <Suspense fallback={null}>
      <div className="info-Card">
        <Card className="mx-2 max-w-5xl flex w-full overflow-visible">
          <Card.Section>
            <BackgroundImage
              src={"/static/Film/bg.png"}
              radius="sm"
              style={{ height: "400px" }}
            >
              <Stack
                h="100%"
                align="stretch"
                justify="space-between"
                gap="md"
                style={{ width: "100%" }}
              >
                <Group p="sm" justify="flex-end">
                  <ActionIcon
                    component={Link}
                    href={pathname}
                    radius="lg"
                    variant="filled"
                    color="gray.6"
                  >
                    {close}
                  </ActionIcon>
                </Group>
                <Group justify="flex-start" p="sm">
                  <Stack gap="md">
                    <Text size="xl" c="white">
                      {trackData.Title}
                    </Text>
                    <Group>
                      <Button
                        component={Link}
                        href={`/watch/${trackData.trackRef}`}
                        color="gray"
                        size="md"
                        variant="light"
                        className="bg-gray-200/90 text-black hover:bg-gray-200/80 hover:text-slate-800/90"
                        leftSection={<CiPlay1 size={30} />}
                      >
                        Play
                      </Button>
                      <ListButton trackref={prop.id} />
                      <LikeButton trackref={trackData.trackRef} />
                    </Group>
                  </Stack>
                </Group>
              </Stack>
            </BackgroundImage>
          </Card.Section>
          <Card.Section>
            <Grid p="md">
              <Grid.Col span={{ base: 12, md: 6, lg: 8 }}>
                <Group p="sm" justify="flex-start">
                  <Text size="sm">{trackData.Year}</Text>
                  <Text size="sm">{trackData.Runtime}</Text>
                  <Badge variant="outline" size="xs" radius="xs" color="white">
                    HDD
                  </Badge>
                </Group>
                <Group p="sm" justify="flex-start" align="center">
                  <Badge variant="outline" size="md" radius="xs" color="white">
                    {trackData.Rated}
                  </Badge>
                  <Text size="xs" className="wrap w-3/4">
                    {ratingMaturity[trackData.Rated].description}
                  </Text>
                </Group>
                <Group p="sm" justify="flex-start">
                  <Text>{trackData.Plot}</Text>
                </Group>
              </Grid.Col>
              <Grid.Col span={{ base: 12, md: 6, lg: 3 }}>
                <Group p="sm" justify="flex-end" grow>
                  <Stack>
                    <Text c="dimmed">
                      Cast: <a className="text-white">{trackData.Actors}</a>
                    </Text>
                    <Text c="dimmed">
                      Genres: <a className="text-white">{trackData.Genre}</a>
                    </Text>
                  </Stack>
                </Group>
              </Grid.Col>
            </Grid>
          </Card.Section>
        </Card>
      </div>
    </Suspense>
  );
};

export { InfoCard };
