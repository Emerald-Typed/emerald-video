"use client";
import { create } from "zustand";

type SearchState = {
  searching: boolean;
  toggleSearch: () => void;
  value: string;
  setValue: (val: string) => void;
  originalPath: string;
  setOriginalPath: (path: string) => void;
};

const useSearchStore = create<SearchState>((set) => ({
  searching: false,
  toggleSearch: () => set((state) => ({ searching: !state.searching })),
  value: "",
  setValue: (val) => set({ value: val }),
  originalPath: "/browse",
  setOriginalPath: (path: string) =>
    set((state) => {
      if (state.searching) {
        set({ value: "", searching: false });
      }
      return { originalPath: path };
    }),
}));

export { useSearchStore };
