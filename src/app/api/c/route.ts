"use server";
import { type NextRequest, NextResponse } from "next/server";
import carouselData from "@/lib/GenreCarousel.json";

export async function GET(request: NextRequest) {
  const searchParams = request.nextUrl.searchParams;
  const pageRef = searchParams.get("pageRef");
  console.log(pageRef);

  if (
    pageRef === "default" ||
    pageRef === "latest" ||
    pageRef === "movies" ||
    pageRef === "tv" ||
    pageRef === "mini"
  ) {
    if (pageRef === "default") {
      return NextResponse.json(carouselData.default);
    }
    if (pageRef === "latest") {
      return NextResponse.json(carouselData.latest);
    }
    if (pageRef === "movies") {
      return NextResponse.json(carouselData.movies);
    }
    if (pageRef === "tv") {
      return NextResponse.json(carouselData.tv);
    }
    if (pageRef === "mini") {
      return NextResponse.json(carouselData.Mini);
    }
  } else {
    return new Response(JSON.stringify({ error: "Category not found" }), {
      status: 404,
      headers: { "Content-Type": "application/json" },
    });
  }
}
