import HeaderLogin from "@/app/components/Nav/HeaderLogin";
import { Skeleton } from "@mantine/core";
const SkelBody = () => {
  return (
    <>
      <HeaderLogin />
      <Skeleton height={"100vh"} width="100%" />
    </>
  );
};

export { SkelBody };
