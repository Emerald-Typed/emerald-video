import { useFormState, useFormStatus } from "react-dom";
import { signup } from "@/Next-Auth/AuthActions";
import {
  Button,
  Stack,
  PasswordInput,
  TextInput,
  Title,
  Checkbox,
  Text,
} from "@mantine/core";

export function SignupForm() {
  const [state, action] = useFormState(signup, undefined);
  return (
    <form action={action} className="text-emerald-950">
      <Stack align="stretch" justify="center" gap="md">
        <Title size="h2" c="emerald.6" pt="sm" fw={700}>
          Sign Up
        </Title>
        {state?.message && (
          <Stack pt="md" align="center" justify="center" gap="md">
            <Text c="red.9">{state.message}</Text>
          </Stack>
        )}
        <TextInput
          c="emerald.6"
          name="name"
          label="Name"
          description="Requirements"
          placeholder="Full Name"
          error={state?.errors?.name}
        />

        <TextInput
          c="emerald.6"
          name="email"
          label="Email"
          description="Enter a valid Email address"
          placeholder="Full Name"
          error={state?.errors?.email}
        />
        <PasswordInput
          c="emerald.6"
          name="password"
          label="Password"
          description="Requirements"
          placeholder="Password"
          error={state?.errors?.password && "Password requirements not met"}
        />
        {state?.errors?.password && (
          <Stack align="stretch" justify="center" gap="sm">
            <Text c="red.9">Password must:</Text>
            {state.errors.password.map((error) => (
              <li key={error} className="px-5 text-red-600">
                {error}
              </li>
            ))}
          </Stack>
        )}
        <SubmitButton />
      </Stack>
    </form>
  );
}

function SubmitButton() {
  const { pending } = useFormStatus();

  return (
    <Button color="emerald.6" disabled={pending} type="submit" w="min">
      Submit
    </Button>
  );
}
