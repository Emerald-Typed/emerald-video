import { Skeleton } from "@mantine/core";
const SkeletonCard = () => {
  return (
    <Skeleton
      style={{ width: "auto", height: "200px", minWidth: "200px" }}
      radius="md"
      p="md"
    />
  );
};
export { SkeletonCard };
