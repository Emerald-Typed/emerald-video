"use client";
import { Container, Group, Stack, Anchor, Badge, Grid } from "@mantine/core";
import { FaInstagram, FaFacebook, FaTwitter, FaYoutube } from "react-icons/fa6";
//make grid
function Footer() {
  return (
    <footer className="bg-emerald-950 py-2">
      <Container size="lg" className="py-2">
        <Group h="30" justify="flex-start" gap="sm" m="lg">
          <FaInstagram className="text-gray-200" />
          <FaFacebook className="text-gray-200" />
          <FaTwitter className="text-gray-200" />
          <FaYoutube className="text-gray-200" />
        </Group>
        <Group justify="space-between" gap="sm">
          <Grid className="w-full p-2">
            <Grid.Col span={{ base: 6, lg: 3 }}>
              <Stack>
                <Anchor
                  href="https://emerald-typed.vercel.app/"
                  target="_blank"
                  c="gray.1"
                >
                  Audio Description
                </Anchor>
                <Anchor
                  href="https://emerald-typed.vercel.app/"
                  target="_blank"
                  c="gray.1"
                >
                  Investor Relations
                </Anchor>
                <Anchor
                  href="https://emerald-typed.vercel.app/"
                  target="_blank"
                  c="gray.1"
                >
                  Privacy
                </Anchor>
                <Anchor
                  href="https://emerald-typed.vercel.app/"
                  target="_blank"
                  c="gray.1"
                >
                  Contact Us
                </Anchor>
              </Stack>
            </Grid.Col>
            <Grid.Col span={{ base: 6, lg: 3 }}>
              <Stack>
                <Anchor
                  href="https://emerald-typed.vercel.app/"
                  target="_blank"
                  c="gray.1"
                >
                  Help Center
                </Anchor>
                <Anchor
                  href="https://emerald-typed.vercel.app/"
                  target="_blank"
                  c="gray.1"
                >
                  Jobs
                </Anchor>
                <Anchor
                  href="https://emerald-typed.vercel.app/"
                  target="_blank"
                  c="gray.1"
                >
                  Legal Notices
                </Anchor>
                <Anchor
                  href="https://emerald-typed.vercel.app/"
                  target="_blank"
                  c="gray.1"
                >
                  Do Not Sell or Share My Personal Information
                </Anchor>
              </Stack>
            </Grid.Col>
            <Grid.Col span={{ base: 6, lg: 3 }}>
              <Stack>
                <Anchor
                  href="https://emerald-typed.vercel.app/"
                  target="_blank"
                  c="gray.1"
                >
                  Gift Cards
                </Anchor>
                <Anchor
                  href="https://emerald-typed.vercel.app/"
                  target="_blank"
                  c="gray.1"
                >
                  Netflix Shop
                </Anchor>
                <Anchor
                  href="https://emerald-typed.vercel.app/"
                  target="_blank"
                  c="gray.1"
                >
                  Cookie Preferences
                </Anchor>
                <Anchor
                  href="https://emerald-typed.vercel.app/"
                  target="_blank"
                  c="gray.1"
                >
                  Ad Choices
                </Anchor>
              </Stack>
            </Grid.Col>
            <Grid.Col span={{ base: 6, lg: 3 }}>
              <Stack>
                <Anchor
                  href="https://emerald-typed.vercel.app/"
                  target="_blank"
                  c="gray.1"
                >
                  Media Center
                </Anchor>
                <Anchor
                  href="https://emerald-typed.vercel.app/"
                  target="_blank"
                  c="gray.1"
                >
                  Terms of Use
                </Anchor>
                <Anchor
                  href="https://emerald-typed.vercel.app/"
                  target="_blank"
                  c="gray.1"
                >
                  Corporate Information
                </Anchor>
              </Stack>
            </Grid.Col>
          </Grid>
        </Group>
        <Group justify="flex-start" gap="sm" mt="lg">
          <Stack>
            <Badge variant="outline" color="gray">
              Service Code
            </Badge>
            <Anchor
              href="https://emerald-typed.vercel.app/"
              target="_blank"
              c="gray.1"
            >
              © 1998-2024 Emerald Typed, Inc.
            </Anchor>
          </Stack>
        </Group>
      </Container>
    </footer>
  );
}

export default Footer;
