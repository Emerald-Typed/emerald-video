"use client";

import { SinglularCarousel } from "@/app/components/Carousel/SinglularCarousel";
import { Suspense } from "react";
import { MediaCategory } from "@/lib/types";

function Carousels({ Categories }: { Categories: MediaCategory[] }) {
  return Categories.map((category, index) => (
    <Suspense key={index}>
      <SinglularCarousel key={index} Slide={category} />
    </Suspense>
  ));
}

export { Carousels };
