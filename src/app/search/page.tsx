"use client";
import { useSearchParams } from "next/navigation";
import { useEffect, useState, Suspense } from "react";
import Header from "@/app/components/Nav/Header";
import { Container, Title, Text } from "@mantine/core";

const Search = () => {
  return (
    <div className="min-h-screen py-10 pt-[65px] w-full px-2">
      <Header />
      <Container
        fluid
        className="my-2 py-2 space-y-2 bg-gradient-to-r rounded-md from-emerald-500 to-emerald-800 text-white"
      >
        <Title order={1}>Search Emerald-Video</Title>
      </Container>
      <Suspense fallback={<p>Searching For: </p>}>
        <SearchValue />
      </Suspense>
    </div>
  );
};
export default Search;

const SearchValue = () => {
  const searchParams = useSearchParams();
  const QueryValue = searchParams.get("q");
  const [search, SetSearch] = useState("");
  useEffect(() => {
    if (QueryValue) {
      SetSearch(QueryValue);
      // const delay = setTimeout(() => {
      //   SetSearch(QueryValue);
      // }, 400); // 400ms delay
      //Would usually do prisma request here but for demo and db cost reasons I'm not
      //It would have been a simple prisma findMany query with a limit for pagination
      // return () => clearTimeout(delay);
    }
  }, [QueryValue]);

  return (
    <Container>
      <Text c="dimmed">
        Loading Results for{" "}
        <span className="text-2xl text-white font-bold">{search}...</span>
      </Text>
      <Text c="dimmed">
        Query the API for finaical reasons but above would be query param search
        delay 400ms
      </Text>
    </Container>
  );
};
