import { Container, Title, Text, Group, Skeleton } from "@mantine/core";
import { BsChevronRight, BsChevronLeft } from "react-icons/bs";
import { Carousel } from "@mantine/carousel";
import { SkeletonCard } from "@/app/components/Skelatons/SkelCard";

const SkeletonCarousal = () => {
  return (
    <Container size="responsive" p="sm">
      <Group className="text-white group">
        <Title py="sm" order={2}>
          <Skeleton>Loading...</Skeleton>
        </Title>
      </Group>
      <Carousel
        height="185px"
        slideGap="md"
        align="start"
        slideSize={{ lg: "16.666667%", md: "33%", base: "60%" }}
        loop
        withControls={false}
        draggable={false}
      >
        {Array.from({ length: 6 }).map((_, index) => (
          <Carousel.Slide key={index}>
            <SkeletonCard />
          </Carousel.Slide>
        ))}
      </Carousel>
    </Container>
  );
};

export { SkeletonCarousal };
