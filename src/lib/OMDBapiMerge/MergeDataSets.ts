import OriginalDB from "@/lib/OMDBapiMerge/EmeraldDB.json";
// import MatchedMinMock from "@/lib/MatchedMinMock.json";
import { OMDBTrack, OriginalTrack, MergedTrack } from "@/lib/types";
import fs from "fs";
import path from "path";

async function getAndMergeTrackData(): Promise<void> {
  try {
    const mergedResults = await Promise.all(
      OriginalDB.map(async (localTrack: OriginalTrack) => {
        try {
          const response = await fetch(
            `http://www.omdbapi.com/?apikey=${process.env.API_KEY}&i=${localTrack.trackRef}&plot=full`
          );
          if (!response.ok) {
            throw new Error(`Error fetching data for ${localTrack.title}`);
          }

          const omdbData: OMDBTrack = await response.json();
          if (!omdbData || !omdbData.Response || omdbData.Response !== "True") {
            console.error(`OMDB data not found for ${localTrack.title}`);
            return null;
          }

          const {
            title,
            posterUrl,
            description,
            Genre,
            ...filteredLocalTrack
          } = localTrack;
          const mergedData: MergedTrack = {
            ...filteredLocalTrack,
            Title: omdbData.Title,
            Genre: omdbData.Genre,
            Year: omdbData.Year,
            Released: omdbData.Released,
            Runtime: omdbData.Runtime,
            Director: omdbData.Director,
            Writer: omdbData.Writer,
            Actors: omdbData.Actors,
            Plot: omdbData.Plot,
            Language: omdbData.Language,
            Country: omdbData.Country,
            Awards: omdbData.Awards,
            Ratings: omdbData.Ratings,
            imdbRating: omdbData.imdbRating,
            imdbVotes: omdbData.imdbVotes,
            totalSeasons: omdbData.totalSeasons,
            Type:
              omdbData.Type === localTrack.Type
                ? localTrack.Type
                : omdbData.Type,
          };
          return mergedData;
        } catch (error) {
          console.error(
            `Error fetching data for ${localTrack.trackRef}:`,
            error
          );
          return null;
        }
      })
    );

    const validResults = mergedResults.filter((item) => item !== null);

    console.log("Merged Data:", validResults);

    const outputFilePath = path.resolve(
      process.cwd(),
      "src/lib",
      "MergedTrackData.json"
    );
    fs.writeFileSync(outputFilePath, JSON.stringify(validResults, null, 2));

    console.log(`Merged data written to ${outputFilePath}`);
  } catch (error) {
    console.error("Error in fetching or merging data:", error);
    throw error;
  }
}

getAndMergeTrackData();
