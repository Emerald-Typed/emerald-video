import type { Metadata } from "next";
import { Inter } from "next/font/google";
import { MantineProvider } from "@mantine/core";
import { theme } from "../../theme";
import React from "react";
import "./globals.css";
import "@mantine/carousel/styles.css";
import QueryProvider from "@/React-Query/TanStackQuery";
//zustand replaced providers here
import { Suspense } from "react";
import Footer from "@/app/components/Nav/Footer";
import { SkelBody } from "@/app/components/Skelatons/SkelBody";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Emerald Video V2 w Mantine",
  description: "Totally overhalled UI with Mantine",
};
export default function RootLayout({ children }: any) {
  return (
    <html lang="en">
      <head>
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width, user-scalable=no"
        />
      </head>
      <body>
        <MantineProvider theme={theme} forceColorScheme="dark">
          <QueryProvider>
            <Suspense fallback={<SkelBody />}>{children}</Suspense>
            <Footer />
          </QueryProvider>
        </MantineProvider>
      </body>
    </html>
  );
}
