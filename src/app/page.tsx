"use client";
import { Image, Button, Paper } from "@mantine/core";
import { SigninForm } from "@/app/components/AuthMock/SignIn";
import Link from "next/link";
import HeaderLogin from "./components/Nav/HeaderLogin";

const Page = () => {
  return (
    <div className="relative h-screen bg-emerald-600/80">
      <Image
        className="absolute inset-0 opacity-60 object-cover w-full h-full"
        src="../static/landing.jpg"
        alt="Landing Background"
      />
      <HeaderLogin />
      <div className="flex items-center justify-center h-full relative z-10">
        <Paper
          shadow="xs"
          pb="sm"
          px="md"
          className="lg:max-w-2xl md:max-w-xl max-w-sm w-full"
        >
          <SigninForm />
          <Button
            component={Link}
            mt="sm"
            fullWidth
            color="emerald.6"
            href="/signup"
          >
            Sign Up
          </Button>
        </Paper>
      </div>
    </div>
  );
};
export default Page;
