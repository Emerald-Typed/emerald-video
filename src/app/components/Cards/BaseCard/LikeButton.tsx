"use client";
import { useHover } from "@mantine/hooks";
import { ActionIcon, Box } from "@mantine/core";
import { HandleAptitude, fetchUserAptitude } from "@/Prisma/UserActions";
import {
  RiThumbUpFill,
  RiThumbDownFill,
  RiArrowUpDoubleFill,
} from "react-icons/ri";
import { useState } from "react";
import { useQuery } from "@tanstack/react-query";
type Loading = "loading" | "error" | null;
type Aptitude = "like" | "dislike" | "doubleLike" | "N/A";

const LikeIcon = <RiThumbUpFill size={15} />;
const DislikeIcon = <RiThumbDownFill size={15} />;
const DoubleLikeIcon = <RiArrowUpDoubleFill size={20} />;

export function LikeButton({ trackref }: { trackref: string }) {
  const { hovered, ref } = useHover();
  const [iconState, setLoadingState] = useState<Loading>(null);

  const { data: Aptitude, refetch } = useQuery({
    queryKey: ["trackAptitude", trackref],
    queryFn: () => fetchUserAptitude(trackref),
    staleTime: Infinity,
  });

  const handleClick = async (aptitude: "like" | "dislike" | "doubleLike") => {
    setLoadingState("loading");
    try {
      console.log("aptitude", aptitude);
      await HandleAptitude(trackref, aptitude);
      await refetch();
    } catch (error) {
      setLoadingState("error");
    } finally {
      setLoadingState(null);
    }
  };
  const colorCheck = (aptitude: Aptitude) => {
    return Aptitude === aptitude ? "emerald.6" : "gray.6";
  };

  return (
    <Box
      ref={ref}
      style={{
        position: "relative",
        width: "auto",
        display: "inline-flex",
        alignItems: "center",
        backgroundColor: "transparent",
        borderRadius: "8px",
        zIndex: 1,
      }}
    >
      <ActionIcon
        radius="lg"
        variant="filled"
        color={colorCheck("like")}
        style={{
          zIndex: 0,
        }}
        onClick={() => handleClick("like")}
        loading={iconState === "loading"}
        disabled={iconState === "loading" || iconState === "error"}
      >
        {LikeIcon}
      </ActionIcon>

      <Box
        style={{
          borderRadius: "8px",
          backgroundColor: hovered ? "#059669" : "transparent",
          position: "absolute",
          top: "-.8rem",
          left: "-.3rem",
          display: "flex",
          gap: "0.5rem",
          padding: "0.3rem",
          marginTop: "0.5rem",
          transition: "all 0.45s ease",
          opacity: hovered ? 2 : 0,
        }}
      >
        <ActionIcon
          onClick={() => handleClick("like")}
          loading={iconState === "loading"}
          disabled={iconState === "loading" || iconState === "error"}
          radius="lg"
          variant="filled"
          color={colorCheck("like")}
        >
          {LikeIcon}
        </ActionIcon>
        <ActionIcon
          loading={iconState === "loading"}
          disabled={iconState === "loading" || iconState === "error"}
          onClick={() => handleClick("dislike")}
          radius="lg"
          variant="filled"
          color={colorCheck("dislike")}
        >
          {DislikeIcon}
        </ActionIcon>
        <ActionIcon
          loading={iconState === "loading"}
          disabled={iconState === "loading" || iconState === "error"}
          onClick={() => handleClick("doubleLike")}
          radius="lg"
          variant="filled"
          color={colorCheck("doubleLike")}
        >
          {DoubleLikeIcon}
        </ActionIcon>
      </Box>
    </Box>
  );
}
