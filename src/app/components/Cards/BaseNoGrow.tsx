"use client";
import {
  Badge,
  BackgroundImage,
  Stack,
  Group,
  Text,
  Image,
  Card,
  ActionIcon,
} from "@mantine/core";
import { Suspense } from "react";
import { SkelLike } from "@/app/components/Skelatons/SkelLike";
import { RiPlayCircleFill, RiArrowDownSLine } from "react-icons/ri";
import { useSuspenseQuery } from "@tanstack/react-query";
import { fetchTrack } from "@/React-Query/TypedFetch";
import { ListButton } from "@/app/components/Cards/BaseCard/ListButton";
import { LikeButton } from "@/app/components/Cards/BaseCard/LikeButton";
import { TrackBadge } from "@/app/components/Cards/BaseCard/TrackBadge";
import Link from "next/link";
import EmeraldIcon from "/public/static/Film/Ew.png";
type BadgeOptions = {
  "Top 10": number | undefined;
  "Oscar Winner": boolean;
  "Oscar Nominee": boolean;
  "New Season": boolean;
  "Recently Added": string;
};

const play = <RiPlayCircleFill size={20} />;
const more = <RiArrowDownSLine size={20} />;

function BaseCardStatic({ trackref }: { trackref: string }) {
  const { data: trackData } = useSuspenseQuery({
    queryKey: ["card", trackref],
    queryFn: () => fetchTrack(trackref),
    staleTime: Infinity,
  });

  const badgeOptions: BadgeOptions = {
    "Top 10": trackData.Top10M || trackData.Top10S,
    "Oscar Winner": trackData.oscarWinner,
    "Oscar Nominee": trackData.oscarNominee,
    "New Season":
      trackData.Type === "series" &&
      typeof trackData.Year === "string" &&
      (trackData.Year.endsWith("2024") ||
        trackData.Year.endsWith("2025") ||
        trackData.Year.startsWith("2024")),
    "Recently Added": trackData.dateAdded,
  };
  const badgePriority = [
    "Oscar Winner",
    "Oscar Nominee",
    "Top 10",
    "New Season",
    "Recently Added",
  ];
  const badgeType = badgePriority.find(
    (type) => badgeOptions[type as keyof BadgeOptions]
  );

  return (
    <Card key={trackData.trackRef}>
      <Card.Section>
        <BackgroundImage
          className="bgCard"
          src={trackData.cardImg || "/static/Film/bg.png"}
          radius="sm"
        >
          <Stack
            h="100%"
            align="stretch"
            justify="space-between"
            gap="md"
            style={{ width: "100%" }}
          >
            <Group justify="space-between">
              {trackData.original ? (
                <Badge color="emerald" size="lg" radius="xs">
                  <Image h={20} w="auto" fit="cover" src={EmeraldIcon.src} />
                </Badge>
              ) : (
                <br />
              )}
            </Group>
            <Group justify="center" className="md:flex  hidden">
              {badgeType && (
                <TrackBadge
                  badgeType={badgeType}
                  dateAdded={trackData.dateAdded}
                />
              )}
            </Group>
          </Stack>
        </BackgroundImage>
      </Card.Section>
      <Card.Section className="h-[100] block">
        <Stack justify="space-between" p="sm">
          <Group justify="space-between">
            <Group justify="flex-start">
              <ActionIcon
                component={Link}
                href={`/watch/${trackData.trackRef}`}
                radius="lg"
                variant="filled"
                color="emerald.6"
              >
                {play}
              </ActionIcon>
              <ListButton trackref={trackData.trackRef} />
              <Suspense fallback={<SkelLike />}>
                <LikeButton trackref={trackData.trackRef} />
              </Suspense>{" "}
            </Group>
            <Group justify="flex-end">
              <ActionIcon
                component={Link}
                href={`?jbv=${trackData.trackRef}`}
                radius="lg"
                variant="filled"
                color="gray.6"
                style={{ zIndex: 1 }}
              >
                {more}
              </ActionIcon>
            </Group>
          </Group>
          <Group justify="flex-start">
            <Badge variant="outline" size="xs" radius="xs" color="white">
              {trackData.Rated}
            </Badge>
            <Text size="sm">1H 35M</Text>
            <Badge variant="outline" size="xs" radius="xs" color="white">
              HDD
            </Badge>
          </Group>

          <Text size="sm" className="whitespace-nowrap truncate">
            {trackData.Genre}
          </Text>
        </Stack>
      </Card.Section>
    </Card>
  );
}

export { BaseCardStatic };
