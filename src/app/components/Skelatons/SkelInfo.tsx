"use client";
import { CiPlay1 } from "react-icons/ci";
import { RiThumbUpFill, RiCloseCircleLine } from "react-icons/ri";
import {
  Button,
  Skeleton,
  BackgroundImage,
  Grid,
  Stack,
  Group,
  Card,
  ActionIcon,
  Text,
} from "@mantine/core";
import Link from "next/link";

const close = <RiCloseCircleLine size={30} />;

const SkelInfo = () => {
  return (
    <div className="info-Card">
      <Card className="mx-2 max-w-5xl flex w-full overflow-visible">
        <Card.Section>
          <Skeleton style={{ zIndex: 0 }} height={400}>
            <Stack
              h="100%"
              align="stretch"
              justify="space-between"
              gap="md"
              style={{ width: "100%" }}
            >
              <Group p="sm" justify="flex-end">
                <ActionIcon
                  style={{ zIndex: 1000 }}
                  component={Link}
                  href="/Browse"
                  radius="lg"
                  variant="filled"
                  color="gray.6"
                >
                  {close}
                </ActionIcon>
              </Group>
              <Group justify="flex-start" p="sm">
                <Stack style={{ zIndex: 1000 }} gap="md">
                  <Skeleton
                    style={{ zIndex: 1000 }}
                    animate={false}
                    height={30}
                    width={220}
                  />
                  <Group p={0} justify="flex-start">
                    <Skeleton animate={false} height={30} width={80} />
                    <Skeleton animate={false} height={30} width={50} />
                    <Skeleton animate={false} height={30} width={50} />
                  </Group>
                </Stack>
              </Group>
            </Stack>
          </Skeleton>
        </Card.Section>
        <Card.Section>
          <Grid p="md">
            <Grid.Col span={{ base: 12, md: 6, lg: 8 }}>
              <Group p="sm" justify="flex-start">
                <Skeleton height={25} width={40} />
                <Skeleton height={25} width={40} />
                <Skeleton height={20} width={40} />
              </Group>
              <Group p="sm" justify="flex-start" align="center">
                <Skeleton height={20} width={40} />
                <Skeleton height={25} width="100%" />
              </Group>
              <Group p="sm" justify="flex-start">
                <Skeleton height={40} width="100%" />
              </Group>
            </Grid.Col>
            <Grid.Col span={{ base: 12, md: 6, lg: 3 }}>
              <Group p="sm" justify="flex-end" grow>
                <Stack>
                  <Text c="dimmed">
                    <Skeleton height="60px" width="100%" />
                  </Text>
                  <Text c="dimmed">
                    <Skeleton height="60px" width="100%" />
                  </Text>
                </Stack>
              </Group>
            </Grid.Col>
          </Grid>
        </Card.Section>
      </Card>
    </div>
  );
};

export { SkelInfo };
