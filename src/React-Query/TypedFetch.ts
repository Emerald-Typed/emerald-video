const API_BASE_URL =
  process.env.NODE_ENV === "development"
    ? "http://localhost:3000"
    : process.env.NEXT_PUBLIC_URL;
import { MediaCategory, MergedTrack } from "@/lib/types";
//Client for suspseful fetch
export async function fetchCarousel(pageRef: string): Promise<MediaCategory[]> {
  const response = await fetch(`${API_BASE_URL}/api/c?pageRef=${pageRef}`);
  return await response.json();
}

export async function fetchTrack(trackRef: string): Promise<MergedTrack> {
  const response = await fetch(`${API_BASE_URL}/api/m?trackRef=${trackRef}`);
  return await response.json();
}

export async function fetchTrackLong(trackRef: string): Promise<MergedTrack> {
  const response = await fetch(
    `${API_BASE_URL}/api/m?trackRef=${trackRef}&plot=full`
  );
  return await response.json();
}
