"use server";
import { signIn } from "@/Next-Auth/auth";
import { redirect } from "next/navigation";
import { z } from "zod";
import bcrypt from "bcryptjs";
import client from "@/Prisma/PrismaClientInstance";
import { AuthError } from "next-auth";
const credentialsSchema = z.object({
  email: z.string().email({ message: "Please enter a valid email." }).trim(),
  password: z
    .string()
    .min(6, { message: "Password must be at least 6 characters long." })
    .trim(),
});

export async function authenticateSignIn(
  prevState: FormStateSignIn,
  formData: FormData
): Promise<FormStateSignIn> {
  const data = {
    email: formData.get("email") as string,
    password: formData.get("password") as string,
  };
  const parsedCredentials = credentialsSchema.safeParse(data);
  if (!parsedCredentials.success) {
    return {
      errors: parsedCredentials.error.flatten().fieldErrors,
    };
  }
  try {
    await signIn("credentials", {
      email: parsedCredentials.data.email,
      password: parsedCredentials.data.password,
      redirect: false,
    });
  } catch (error) {
    console.error("Error during signin:", error);
    if (error instanceof AuthError) {
      const trimmedErrorMessage = error.message
        .replace("Error: ", "")
        .split(".")[0];
      return { message: trimmedErrorMessage };
    }
    return { message: `${error}` };
  }
  redirect("/browse");
}

const registerSchema = z.object({
  name: z
    .string()
    .min(2, { message: "Name must be at least 2 characters long." }),
  email: z.string().email({ message: "Please enter a valid email." }).trim(),
  password: z
    .string()
    .min(6, { message: "Password must be at least 6 characters long." })
    .trim(),
});
export async function signup(
  prevState: FormStateSignUp,
  formData: FormData
): Promise<FormStateSignUp> {
  const data = {
    name: formData.get("name") as string,
    email: formData.get("email") as string,
    password: formData.get("password") as string,
  };
  const parsedCredentials = registerSchema.safeParse(data);
  if (!parsedCredentials.success) {
    console.log("Invalid credentials:", parsedCredentials.error);
    if (!parsedCredentials.success) {
      return {
        errors: parsedCredentials.error.flatten().fieldErrors,
      };
    }
  }
  const { name, email, password } = parsedCredentials.data;
  const hashedPassword = await bcrypt.hash(password, 10);
  console.log("Hashed password:", hashedPassword);
  try {
    const user = await client.user.upsert({
      where: { email },
      update: {
        name,
        password: hashedPassword,
      },
      create: {
        name,
        email,
        password: hashedPassword,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    });
    if (!user) {
      return { message: "Failed to create user." };
    }
  } catch (error) {
    console.error("Error during signup:", error);
    return { message: `${error}` };
  }
  redirect("/");
}

type FormStateSignUp =
  | {
      errors?: {
        name?: string[];
        email?: string[];
        password?: string[];
      };
      message?: string;
    }
  | undefined;

type FormStateSignIn =
  | {
      errors?: {
        email?: string[];
        password?: string[];
      };
      message?: string;
    }
  | undefined;
