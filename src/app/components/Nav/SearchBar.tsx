"use client";
import { useEffect, useRef } from "react";
import { FaMagnifyingGlass, FaXmark } from "react-icons/fa6";
import { Input, Box, Transition } from "@mantine/core";
import { useRouter } from "next/navigation";
import { usePathname } from "next/navigation";
import { useSearchStore } from "@/app/components/Helpers/searchProvider";

const SearchBar = () => {
  const {
    searching,
    toggleSearch,
    value,
    setValue,
    originalPath,
    setOriginalPath,
  } = useSearchStore();

  const router = useRouter();
  const pathName = usePathname();

  const originalPathRef = useRef(originalPath);

  useEffect(() => {
    if (!pathName.startsWith("/search")) {
      setOriginalPath(pathName);
      originalPathRef.current = pathName;
    }
  }, [pathName, setOriginalPath]);

  useEffect(() => {
    if (value.trim()) {
      console.log(`Searching for ${value}`);
      router.push(`/search?q=${encodeURIComponent(value)}`);
    } else {
      console.log("Returning to:", originalPathRef.current);
      router.push(originalPathRef.current);
    }
  }, [value, router]);
  return (
    <>
      <FaMagnifyingGlass
        size={20}
        onClick={toggleSearch}
        className={searching ? "hidden" : ""}
        style={{ color: "white" }}
      />
      <Transition
        transition="slide-left"
        duration={200}
        mounted={searching}
        onExited={() => setValue("")}
      >
        {(styles) => (
          <Box
            className={`flex items-center justify-start space-x-2 text-white min-w-[40px] max-w-[300px] px-2 rounded-lg transition-color duration-500 ${
              searching
                ? "DarkBG outline outline-1 outline-white w-full"
                : "hidden"
            }`}
            h={40}
          >
            <FaMagnifyingGlass
              size={20}
              onClick={toggleSearch}
              className={searching ? "" : "hidden"}
            />
            <Input
              value={value}
              onChange={(event) => setValue(event.currentTarget.value)}
              style={styles}
              variant="unstyled"
              className={!searching ? "hidden" : "w-full"}
              placeholder="Search for Title"
              rightSectionPointerEvents="all"
              rightSection={
                <FaXmark
                  onClick={() => setValue("")}
                  className="text-white"
                  size={20}
                />
              }
            />
          </Box>
        )}
      </Transition>
    </>
  );
};

export { SearchBar };
