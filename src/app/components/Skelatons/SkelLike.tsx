import { ActionIcon, Box } from "@mantine/core";
import { RiThumbUpFill } from "react-icons/ri";
export { Box, ActionIcon } from "@mantine/core";
const LikeIcon = <RiThumbUpFill size={15} />;

const SkelLike = () => {
  return (
    <Box
      style={{
        position: "relative",
        width: "auto",
        display: "inline-flex",
        alignItems: "center",
        backgroundColor: "transparent",
        borderRadius: "8px",
        zIndex: 1,
      }}
    >
      <ActionIcon
        radius="lg"
        variant="filled"
        color="gray.6"
        style={{
          zIndex: 0,
        }}
        loading={true}
      >
        {LikeIcon}
      </ActionIcon>
    </Box>
  );
};

export { SkelLike };
