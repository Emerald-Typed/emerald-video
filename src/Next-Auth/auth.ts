import NextAuth, { AuthError, NextAuthConfig, User } from "next-auth";
import Credentials from "next-auth/providers/credentials";
/*@ts-ignore */
import client from "@/Prisma/PrismaClientInstance";
import bcrypt from "bcryptjs"; // Use bcryptjs
export const BASE_PATH = process.env.BASE_PATH;

export const AuthConfig: NextAuthConfig = {
  basePath: BASE_PATH,
  pages: {
    signIn: "/",
  },
  secret: process.env.AUTH_SECRET,
  // debug: true,

  providers: [
    Credentials({
      name: "Credentials",
      credentials: {
        email: { label: "Email", type: "email" },
        password: { label: "Password", type: "password" },
      },
      async authorize(credentials): Promise<User | null> {
        const user = await client.user.findUnique({
          where: { email: credentials.email as string },
        });

        if (!user) {
          throw new AuthError("No User Found Matching that Email");
        }
        const validPassword = await bcrypt.compare(
          credentials.password as string,
          user.password
        );
        if (!validPassword) {
          throw new AuthError("Passwords Do Not Match");
        }
        const validatedUser: User = {
          id: user.id.toString(),
          email: user.email,
          name: user.name,
        };

        return validatedUser;
      },
    }),
  ],
  callbacks: {
    async jwt({ token, user }) {
      if (user) {
        token.id = user.id;
      }
      return token;
    },
    async session({ session, token }) {
      if (token) {
        session.user.id = token.id as string;
      }
      return session;
    },
  },
};

export const { handlers, auth, signIn, signOut } = NextAuth(AuthConfig);
