import { Badge } from "@mantine/core";
import { RiAwardFill } from "react-icons/ri";

interface BadgeProps {
  badgeType: string;
  dateAdded?: string;
}

export const TrackBadge = ({ badgeType, dateAdded }: BadgeProps) => {
  const award = <RiAwardFill />;
  let CardBadge;

  if (badgeType === "Recently Added") {
    const dateAddedInstance = new Date(dateAdded || "");
    const today = new Date();
    const timeDiff = today.getTime() - dateAddedInstance.getTime();
    const daysAgo = Math.floor(timeDiff / (1000 * 60 * 60 * 24));

    if (daysAgo > 100) {
      return null;
    }
  }

  switch (badgeType) {
    case "Oscar Winner":
      CardBadge = (
        <Badge color="gray.7" size="lg" radius="sm">
          {award}
          {badgeType}
          {award}
        </Badge>
      );
      break;
    case "Oscar Nominee":
      CardBadge = (
        <Badge color="gray.7" size="lg" radius="sm">
          {award}
          {badgeType}
          {award}
        </Badge>
      );
      break;
    case "Top 10":
    case "New Season":
    case "Recently Added":
      CardBadge = (
        <Badge color="emerald.6" size="lg" radius="sm">
          {badgeType}
        </Badge>
      );
      break;
    default:
      return null;
  }

  return CardBadge;
};
