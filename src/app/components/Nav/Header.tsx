"use client";
import Link from "next/link";
import { useState, useEffect, Suspense } from "react";
import { FaBell, FaPencil, FaUser, FaQuestion } from "react-icons/fa6";
import {
  AppShell,
  Group,
  Burger,
  Button,
  Image,
  Stack,
  HoverCard,
  Transition,
  Avatar,
  Text,
  Box,
  Divider,
  Anchor,
} from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import { usePathname } from "next/navigation";
import { signOut } from "next-auth/react";
import { SearchBar } from "@/app/components/Nav/SearchBar";
import Wink from "/public/static/Wink.png";
import EmeraldLogo from "/public/static/Film/EmeraldVideo.png";
import EmeraldIcon from "/public/static/Film/Ew.png";

const links = [
  { link: "/browse", label: "Home" },
  { link: "/browse/tv", label: "TV Shows" },
  { link: "/browse/movies", label: "Movies" },
  { link: "/latest", label: "New & Popular" },
  { link: "/browse/my-list", label: "My List" },
];

function Header() {
  //Burger menu
  const [opened, { toggle }] = useDisclosure(false);
  //Search bar
  const [scrolled, setScrolled] = useState(false);
  const [active, setActive] = useState<string | null>(null);
  const pathname = usePathname();

  useEffect(() => {
    setActive(pathname);
  }, [pathname]);

  const items = links.map((link) => (
    <Button
      color="gray"
      variant="transparent"
      component={Link}
      key={link.label}
      href={link.link}
      className={`${
        active === link.link ? "font-bold text-white" : "font-light"
      } transition duration-400 transition-color`}
    >
      {link.label}
    </Button>
  ));
  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY > 50) {
        setScrolled(true);
      } else {
        setScrolled(false);
      }
    };
    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <AppShell
      withBorder={false}
      header={{ height: 60 }}
      navbar={{
        width: "250px",
        breakpoint: 0,
        collapsed: { mobile: !opened },
      }}
    >
      <AppShell.Header
        zIndex={1001}
        className={`transition duration-500 px-2 ${
          opened
            ? "bg-emerald-950"
            : scrolled
            ? "bg-emerald-950/80"
            : "bg-transparent"
        }`}
      >
        <Stack align="stretch" h={60} justify="center" gap="xs">
          <Group justify="space-between" wrap="nowrap">
            <Group justify="flex-start" wrap="nowrap">
              <Burger
                opened={opened}
                onClick={toggle}
                color="white"
                className="lg:hidden block"
              />
              <Image
                className="rounded-sm hidden lg:block"
                h={40}
                w="auto"
                src={EmeraldLogo.src}
                alt="Home"
              />
              <Image
                className="rounded-sm block lg:hidden"
                h={30}
                w="auto"
                src={EmeraldIcon.src}
                alt="Home"
              />
              <Group
                className="lg:flex hidden"
                justify="flex-start"
                wrap="nowrap"
              >
                {items}
              </Group>
            </Group>
            <Group justify="flex-end" wrap="nowrap" className="w-full">
              <Suspense>
                <SearchBar />
              </Suspense>
              <FaBell size={20} className="text-white" />
              <HoverCard
                width={320}
                shadow="md"
                withArrow
                openDelay={200}
                closeDelay={400}
              >
                <HoverCard.Target>
                  <Avatar
                    className="hidden lg:block bg-emerald-600"
                    variant="filled"
                    radius="sm"
                    color="emerald.6"
                    src={Wink.src}
                    alt="Winking Me"
                  />
                </HoverCard.Target>
                <HoverCard.Dropdown>
                  <Group p="sm">
                    <FaPencil size={12} className="text-white" />
                    <Anchor c="white">Manage Profiles</Anchor>
                  </Group>
                  <Group p="sm">
                    <FaUser size={12} className="text-white" />
                    <Anchor c="white">Account</Anchor>
                  </Group>
                  <Group p="sm">
                    <FaQuestion size={12} className="text-white" />
                    <Anchor c="white">Help Center</Anchor>
                  </Group>
                  <Divider my="xs" />
                  <Group justify="center">
                    <Anchor c="white" onClick={() => signOut()}>
                      Sign Out
                    </Anchor>
                  </Group>
                </HoverCard.Dropdown>
              </HoverCard>
            </Group>
          </Group>
        </Stack>
        <Transition transition="slide-right" duration={100} mounted={opened}>
          {(styles) => (
            <AppShell.Navbar style={styles} p="md" className="bg-emerald-950">
              <Stack align="flex-start" justify="flex-start" gap="md">
                <Group justify="flex-start" wrap="nowrap" className="w-full">
                  <Avatar
                    rel="preload"
                    variant="filled"
                    radius="sm"
                    color="emerald.6"
                    src={Wink.src}
                  />
                  <span className="flex flex-col">
                    <Text>UserName</Text>
                    <Text size="sm">Switch Profile</Text>
                  </span>
                </Group>
                <Anchor
                  style={{
                    fontWeight: "light",
                    color: "#059669",
                  }}
                  underline="never"
                  className="hover:font-bold transition duration-400 transition-color"
                >
                  Account
                </Anchor>
                <Anchor
                  style={{
                    fontWeight: "light",
                    color: "#059669",
                  }}
                  underline="never"
                  className="hover:font-bold transition duration-400 transition-color"
                >
                  Help Center
                </Anchor>
                <Anchor
                  style={{
                    fontWeight: "light",
                    color: "#059669",
                  }}
                  underline="never"
                  className="hover:font-bold transition duration-400 transition-color"
                  onClick={() => signOut()}
                >
                  Sign Out
                </Anchor>
              </Stack>
              <Divider my="xs" c="white" />
              <Stack align="flex-start" justify="flex-start" gap="md">
                {links.map((link, index) => (
                  <Stack style={{ position: "relative" }} key={index}>
                    {active === link.link && (
                      <Box
                        style={{
                          position: "absolute",
                          left: -20,
                          width: "8px",
                          height: "100%",
                          backgroundColor: "#4ade80",
                          marginRight: 8,
                        }}
                      />
                    )}
                    <Anchor
                      component={Link}
                      href={link.link}
                      styles={{
                        root: {
                          fontWeight: active === link.link ? "bold" : "light",
                          color: active === link.link ? "white" : "gray",
                        },
                      }}
                      className="transition duration-400 transition-color"
                    >
                      {link.label}
                    </Anchor>
                  </Stack>
                ))}
              </Stack>
            </AppShell.Navbar>
          )}
        </Transition>
      </AppShell.Header>
    </AppShell>
  );
}

export default Header;
