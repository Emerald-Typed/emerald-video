"use client";
import Video from "next-video";
import { Button, Group, Title } from "@mantine/core";
import Link from "next/link";
import { FetchSelio } from "@/Prisma/UserActions";
import { useEffect, useState, Suspense } from "react";

//real team has alot more parmas going into custom webplayer for how much viewed and time left last watched resolution and laungage pref at the very least

export default function Page({ params }: { params: { trackID: string } }) {
  const [controls, setControlVisible] = useState(true);
  const [selio, setSelio] = useState<string>();
  let timeoutId: NodeJS.Timeout;
  useEffect(() => {
    const FetchURL = async () => {
      const Seal = await FetchSelio();
      if (Seal) {
        setSelio(Seal);
      }
    };
    FetchURL();
  }, []);
  const onMouseMove = () => {
    setControlVisible(true);

    if (timeoutId) {
      clearTimeout(timeoutId);
    }
    timeoutId = setTimeout(() => {
      setControlVisible(false);
    }, 1000);
  };

  useEffect(() => {
    window.addEventListener("mousemove", onMouseMove);

    return () => {
      window.removeEventListener("mousemove", onMouseMove);
      clearTimeout(timeoutId);
    };
  }, []);
  return (
    <main className="flex min-h-screen flex-col">
      <nav
        className={`sticky top-0 z-50 bg-emerald-950/80 transition-opacity duration-700 ${
          controls ? "opacity-100" : "opacity-0"
        }`}
      >
        <Group>
          <Title
            order={1}
            c="emerald.6"
            className="text-center text-2xl text-emerald-600 mx-auto"
          >
            {params.trackID}
          </Title>
          <Button component={Link} href="/browse" m="sm" color="emerald.6">
            Browse
          </Button>
        </Group>
      </nav>
      <span className="z-0 -mt-20 flex-grow bg-black flex justify-center items-center">
        <Suspense fallback={<>Loadihng</>}>
          <VideoComp src={selio as string} />
        </Suspense>
      </span>
    </main>
  );
}

const VideoComp = (selio: { src: string }) => {
  return (
    <Video
      src={selio.src}
      style={{ maxWidth: "200rem" }}
      accentColor="#4ade80"
    />
  );
};
