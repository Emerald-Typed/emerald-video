"use client";
import { useSearchParams } from "next/navigation";
import { Carousels } from "@/app/components/Carousel/MainCarousels";
import { Suspense } from "react";
import { InfoCard } from "@/app/components/Cards/InfoCard";
import { BGVideo } from "./BGVideo";
import { SkeletonCarousal } from "../Skelatons/SkelCaro";
import { Skeleton } from "@mantine/core";
import { useSuspenseQuery } from "@tanstack/react-query";
import { fetchCarousel } from "@/React-Query/TypedFetch";
import { useVideoStore } from "../Helpers/videoProvider";
export default function Default() {
  const searchParams = useSearchParams();
  const id = searchParams.get("jbv");
  const { videoData } = useVideoStore();
  const { data: categories } = useSuspenseQuery({
    queryKey: ["carousels", videoData.key],
    queryFn: () => fetchCarousel(videoData.key),
    staleTime: Infinity,
  });

  return (
    <div className="z-0 -mt-16 w-full pb-10">
      <Suspense fallback={<Skeleton height={"80vh"} width="100%" />}>
        <BGVideo videoData={videoData} />
      </Suspense>
      <Suspense fallback={<SkeletonCarousal />}>
        {categories && videoData.key && <Carousels Categories={categories} />}
      </Suspense>
      {id && (
        <Suspense fallback={null}>
          <InfoCard id={id} />
        </Suspense>
      )}
    </div>
  );
}
