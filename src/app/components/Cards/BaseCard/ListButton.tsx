"use client ";
import { ActionIcon } from "@mantine/core";
import { useListStore } from "@/app/components/Helpers/listProvider";
import { RiCheckFill, RiAddFill } from "react-icons/ri";
import { addToList, removeFromList } from "@/Prisma/UserActions";
import { useState } from "react";

const added = <RiCheckFill size={20} />;
const add = <RiAddFill size={20} />;

type Loading = "loading" | "error" | null;

export function ListButton({ trackref }: { trackref: string }) {
  const { list, setList } = useListStore();
  const [iconState, setLoadingState] = useState<Loading>(null);
  const isAdded = list.includes(trackref);

  const handleClick = async () => {
    setLoadingState("loading");
    try {
      if (isAdded) {
        await removeFromList(trackref);
        setList(list.filter((item) => item !== trackref));
      } else {
        await addToList(trackref);
        setList([...list, trackref]);
      }
    } catch (error) {
      setLoadingState("error");
    } finally {
      setLoadingState(null);
    }
  };
  return (
    <ActionIcon
      radius="lg"
      variant="filled"
      color="gray.6"
      onClick={handleClick}
      loading={iconState === "loading"}
      disabled={iconState === "loading" || iconState === "error"}
    >
      {isAdded ? added : add}
    </ActionIcon>
  );
}
