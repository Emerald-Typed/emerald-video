"use client";
import { create } from "zustand";
import { fetchUserList } from "@/Prisma/UserActions";

type ListStore = {
  list: string[];
  setList: (list: string[]) => void;
  refreshList: () => Promise<void>;
};

const useListStore = create<ListStore>((set) => ({
  list: [],
  setList: (newList) => set({ list: newList }),
  refreshList: async () => {
    try {
      const userList = await fetchUserList();
      set({ list: userList });
    } catch (error) {
      console.error("Error fetching user list:", error);
    }
  },
}));

export { useListStore };
