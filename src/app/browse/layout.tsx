"use client";
import { ReactNode, Suspense, useEffect } from "react";
import Header from "@/app/components/Nav/Header";
import Footer from "@/app/components/Nav/Footer";
import { useListStore } from "@/app/components/Helpers/listProvider";
import { useVideoStore } from "../components/Helpers/videoProvider";
import { usePathname } from "next/navigation";

export default function Page({ children }: { children: ReactNode }) {
  const pathname = usePathname();
  const { refreshList } = useListStore();
  const { fetchVideoSource } = useVideoStore();
  useEffect(() => {
    refreshList();
  }, [refreshList]);
  useEffect(() => {
    fetchVideoSource(pathname);
  }, [pathname]);
  return (
    <>
      <Header />
      {children}
    </>
  );
}
