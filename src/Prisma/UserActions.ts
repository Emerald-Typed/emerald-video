"use server";
import client from "@/Prisma/PrismaClientInstance";
import { auth } from "@/Next-Auth/auth";
import { unstable_cache, revalidateTag } from "next/cache";

const fetchUserListCached = unstable_cache(
  async (userId) => {
    const user = await client.viewList.findUnique({
      where: { userId: userId },
      select: { tracks: { select: { trackRef: true } } },
    });

    return user?.tracks.map((track) => track.trackRef) || [];
  },
  ["list", "userId"],
  {
    tags: ["list"],
  }
);

export const fetchUserList = async () => {
  const session = await auth();
  const userId = session?.user?.id;
  if (!userId) {
    return [];
  }
  return await fetchUserListCached(userId);
};

export const addToList = async (trackRef: string) => {
  const session = await auth();
  const userId = session?.user?.id;
  if (!userId) {
    return { error: "User not authenticated" };
  }
  try {
    await client.viewList.upsert({
      where: { userId: userId },
      update: {
        tracks: {
          connect: {
            trackRef,
          },
        },
      },
      create: {
        userId: userId,
        tracks: {
          connect: {
            trackRef,
          },
        },
      },
    });
  } catch (error) {
    console.error("Error adding track to list:", error);
  } finally {
    revalidateTag("list");
  }
};

export const removeFromList = async (trackRef: string) => {
  const session = await auth();
  const userId = session?.user?.id;
  if (!userId) {
    return { error: "User not authenticated" };
  }
  try {
    await client.viewList.update({
      where: { userId: userId },
      data: {
        tracks: {
          disconnect: {
            trackRef,
          },
        },
      },
    });
  } catch (error) {
    console.error("Error removing track from list:", error);
  } finally {
    revalidateTag("list");
  }
};

const fetchUserAptitudeCached = async (userId: string, trackRef: string) => {
  const cacheTag = `aptitude-${userId}-${trackRef}`;
  return unstable_cache(
    async () => {
      const aptitude = await client.userTrackInteraction.findUnique({
        where: {
          userId_trackRef: {
            userId: userId,
            trackRef: trackRef,
          },
        },
        select: { LikeStatus: true },
      });
      const cachedValue = aptitude?.LikeStatus || "N/A";
      return cachedValue;
    },
    [cacheTag],
    {
      tags: [cacheTag],
    }
  )().then((result) => {
    return result;
  });
};

export const fetchUserAptitude = async (trackRef: string) => {
  const session = await auth();
  const userId = session?.user?.id;

  if (!userId) {
    return { error: "User not authenticated" };
  }
  try {
    return await fetchUserAptitudeCached(userId, trackRef);
  } catch (error) {
    console.error("Error fetching aptitude interaction:", error);
    return { error: "Failed to fetch interaction" };
  }
};

export const HandleAptitude = async (
  trackRef: string,
  aptitude: "like" | "dislike" | "doubleLike"
) => {
  const session = await auth();
  const userId = session?.user?.id;

  if (!userId) {
    return { error: "User not authenticated" };
  }
  try {
    const currentStatus = await fetchUserAptitudeCached(userId, trackRef);

    if (currentStatus !== aptitude) {
      await client.userTrackInteraction.upsert({
        where: {
          userId_trackRef: {
            userId: userId,
            trackRef: trackRef,
          },
        },
        update: {
          LikeStatus: aptitude,
        },
        create: {
          userId: userId,
          trackRef: trackRef,
          LikeStatus: aptitude,
        },
      });
      revalidateTag(`aptitude-${userId}-${trackRef}`);
    }
    return { success: true };
  } catch (error) {
    console.error("Error handling aptitude interaction:", error);
    return { error: "Failed to update or create interaction" };
  }
};

export const FetchSelio = async () => {
  const selioUrl = process.env.NEXT_PUBLIC_BLOB_URL_SEALIO;
  return selioUrl;
};
