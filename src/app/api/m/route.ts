"use server";
//for serving up data cheaper just regugitation json I have seeded in postgreSQL its static so it saves me money this way,
//however it needs to in db for relations on trackrefs like like and in list all ids are concurrent so for this UI example its fine

import { type NextRequest, NextResponse } from "next/server";
import { MergedTrack } from "@/lib/types";
import Tracks from "@/lib/FinalSeedEmerald.json";
import TracksPlot from "@/lib/FinalSeedEmeraldLong.json";

const TrackData: MergedTrack[] = Tracks;
const TracksDataPlot: MergedTrack[] = TracksPlot;

export async function GET(request: NextRequest) {
  const searchParams = request.nextUrl.searchParams;
  const mediaRef = searchParams.get("trackRef");
  const fullPlot = searchParams.get("plot") || "";
  const selectedTracks = fullPlot === "full" ? TracksDataPlot : TrackData;
  if (!Array.isArray(selectedTracks)) {
    console.error("Invalid media format");
    return NextResponse.json(
      { error: "Internal Server Error" },
      { status: 500 }
    );
  }
  const movie = selectedTracks.find((m) => m.trackRef === mediaRef);
  if (movie) {
    return NextResponse.json(movie);
  } else {
    return NextResponse.json({ error: "Track not found" }, { status: 404 });
  }
}
