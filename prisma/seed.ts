import { PrismaClient } from "@prisma/client";
import TrackSeed from "@/lib/FinalSeedEmerald.json";
import TrackSeedLong from "@/lib/FinalSeedEmeraldLong.json";
const prisma = new PrismaClient();
const DefaultList = ["tt0799934", "tt1637706", "tt14544190", "tt0347149"];

import bcrypt from "bcryptjs";

async function main() {
  const password = "password";
  const hashedPassword = await bcrypt.hash(password, 10);

  const TestUser = await prisma.user.upsert({
    where: { email: "test@emerald.io" },
    update: {},
    create: {
      email: "test@emerald.io",
      name: "Kly",
      password: hashedPassword,
    },
  });
  console.log({ TestUser });
  const notification = await prisma.notification.create({
    data: {
      message: "Welcome to the platform, Kly! Enjoy your my app.",
      userId: TestUser.id,
    },
  });
  console.log({ notification });

  const combinedTrackData = TrackSeed.map((track) => {
    const longPlotTrack = TrackSeedLong.find(
      (longTrack) => longTrack.trackRef === track.trackRef
    );
    const trackData = {
      ...track,
      PlotLong: longPlotTrack ? longPlotTrack.Plot : track.Plot,
      dateAdded: new Date(track.dateAdded),
      Released: new Date(track.Released),
      newSeason: Boolean(track.newSeason),
      oscarNominee: Boolean(track.oscarNominee),
      oscarWinner: Boolean(track.oscarWinner),
      Top10M: track.Top10M || null,
      Top10S: track.Top10S || null,
      original: Boolean(track.original),
      Tags: track.Tags || [],
    };
    return trackData;
  });
  for (const trackData of combinedTrackData) {
    const { Ratings, Tags, ...trackWithoutRatingsAndTags } = trackData;
    await prisma.track.upsert({
      where: { trackRef: trackWithoutRatingsAndTags.trackRef },
      update: {
        ...trackWithoutRatingsAndTags,
      },
      create: {
        ...trackWithoutRatingsAndTags,
        Tags: {
          connectOrCreate: Tags.map((tagName) => ({
            where: { name: tagName },
            create: { name: tagName },
          })),
        },
      },
    });
    if (Tags && Tags.length > 0) {
      await prisma.track.update({
        where: { trackRef: trackData.trackRef },
        data: {
          Tags: {
            connectOrCreate: Tags.map((tagName) => ({
              where: { name: tagName },
              create: { name: tagName },
            })),
          },
        },
      });
    }
    if (Ratings) {
      const ratingData = {
        IMDB: Ratings.find((r) => r.Source === "Internet Movie Database")
          ?.Value,
        RottenTom: Ratings.find((r) => r.Source === "Rotten Tomatoes")?.Value,
        Metacritic: Ratings.find((r) => r.Source === "Metacritic")?.Value,
      };
      await prisma.rating.upsert({
        where: { trackRef: trackData.trackRef },
        update: ratingData,
        create: { ...ratingData, trackRef: trackData.trackRef },
      });
    }
  }
}
main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
